import Vue from 'vue'
import Router from 'vue-router'
import FilterPage from '@/pages/FilterPage'
import Content from '@/pages/Content'
import axios from 'axios'

Vue.use(Router)
Vue.prototype.$axios = axios

export default new Router({
  routes: [
    {
      path: '/',
      name: 'FilterPage',
      component: FilterPage
    },
    {
      path: '/content/:id',
      name: 'Content',
      component: Content
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})
